require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "Ping-pong fan community"
    assert_equal full_title("Help"), "Help | Ping-pong fan community"
  end
end